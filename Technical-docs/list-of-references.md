**List of SKA1-SDP reference documents**

**Context:**

The idea of this memo is to list all the relevant and important documents concerning SKA1-SDP efforts. This can help to onboard the new members of the team to get up to the speed. The scope of this document is limited to only activities within SKA and mostly important efforts towards SDP. Most, if not all, of the documents listed in this memo can be found in the ska-sdp.org website. The readers of this document are assumed to have a basic understanding of radio-astronomy imaging and calibration pipelines.

**SKA1-SDP High Level Overview:**

This is a good place to start to get the overall view of the proposed SDP architecture after the CDR in 2019.

http://ska-sdp.org/sites/default/files/attachments/ska-tel-sdp-0000180_02_sdpoverview_part_1_-_signed.pdf

This document provides various components that will reside within SDP and also the data movement structure. It is definitely worth to skim across the document to follow-up the work on the architecture side of SDP.

**SDP Pipeline Workflows:**

This is another interesting document, both for radio-astronomers and HPC experts, that outlines the proposed workflows for various pipelines that will run in SDP. This includes imaging and non-imaging workflows, calibration, fast imaging, _etc_.

http://ska-sdp.org/sites/default/files/attachments/ska-tel-sdp-0000013_07_sdparchitecture_workflows_views.pdf

Each workflow section provides the notes on algorithms, a brief state-of-the-art from existing pipelines and if they can be used in the SDP.

**SDP Parametric Model:**

This document outlines the processing requirements of the SDP based on a parametric model. Essentially, the outcome of the document is the required number of FLOPS based on different parameters like number of major/minor cycles, ingest rate, number of pixels of the image, _etc._, for different components of the pipelines that will run on SDP.

http://ska-sdp.org/sites/default/files/attachments/ska-tel-sdp-0000013_07_sdparchitecture_performancemodel.pdf

The models are implemented using sympy package in ipython and the repository of the code can be found here:

https://gitlab.com/ska-telescope/sdp/ska-sdp-par-model

The documentation of the code is rather slim but the Jupyter notebooks have decent amount of annotations.

The above three documents are part of SDP CDR closeout documentation. There are more documents some prototyping reports, memory requirements, system sizing, _etc._ If interested, the reader can find them here:

http://ska-sdp.org/publications/sdp-cdr-closeout-documentation

**SDP Memos:**

The following list of documents are part of the SDP memo series.

**FFT analysis:**

This memo investigates the computational accuracy and performance of different FFT libraries (FFTW3 and Intel MKL). It also uses CuFFT (CUDA library for FFTs) and a comprehensive comparison for different approaches is presented. It can be an interesting activity to reproduce this work with a recent hardware (the original work had been done in 2016) as it directly fits into co-design vision.

http://ska-sdp.org/sites/default/files/attachments/ska-tel-sdp-0000058_02c_rep_sdpmemofastfouriertransforms_-_signed.pdf

This is another relevant work in terms of co-design, where power efficiency of NVIDIA GPUs are estimated using 1D FFTs. This relatively simple framework can be reproduced with a more recent hardware.

http://ska-sdp.org/sites/default/files/attachments/powerefficiencysdp_part_1_-_signed.pdf

**SDP Computational efficiency:**

This is another work of potential use in terms of co-design activities. This fairly short documents presents a framework to estimate the computational efficiency using roofline models and apply it to future hardware.

http://ska-sdp.org/sites/default/files/attachments/ska-tel-sdp-0000086_c_rep_sdpmemoefficiencyprototyping_-_signed.pdf

**Convolution gridding:**

This work by Numerical Algorithms Group (NAG) presents the computational performance and profiling of convolution gridding on CPUs and GPUs. The document includes code snippets that can be useful for future co-design activities.

http://ska-sdp.org/sites/default/files/attachments/sdp_memo_36.pdf

Using the codes developed by NAG in the above stated work, NVIDIA proposed few modifications to the GPU code and reported performance and profiling metrics in the following work.

http://ska-sdp.org/sites/default/files/attachments/memo_72_gridding_part_1_-_signed_0.pdf

There are more follow-up works from the original work on GPUs and FPGAs.

http://ska-sdp.org/sites/default/files/attachments/memo_73_cheb_part_1_-_signed_0.pdf

http://ska-sdp.org/sites/default/files/attachments/memo_74_fpga_part_1_-_signed_0.pdf

**NVIDIA's studies:**

NVIDIA presented this memo with a very simple gridding code and using different parallelisation techniques and eventually comparing them. This can be another useful work in the co-design context.

http://ska-sdp.org/sites/default/files/attachments/sdp_memo_086_ska-sdp_gridding_on_graphical_processing_units_part_1_-_signed.pdf

The complement work on the degridding side is also done by NVIDIA which can be found here:

http://ska-sdp.org/sites/default/files/attachments/sdp_memo_087_degridding_optimization_for_ska_on_nvidia_gpus_part_1_-_signed.pdf

A basic FFT workload characterization using NVIDIA GPUs is done and presented in

http://ska-sdp.org/sites/default/files/attachments/sdp_memo_088_characterizing_fft_performance_on_gpus_for_ska-sdp_part_1_-_signed.pdf

The following work compares the different convolution methods on GPUs

http://ska-sdp.org/publications/released-sdp-memos-2

**Co-design recommendations:**

This document presents some of the observations and recommendations that are found/made in the context of SDP scaling and benchmark tests.

http://ska-sdp.org/sites/default/files/attachments/sdp_memo_56_scaling_v1.pdf

**Gridding analysis:**

This work is yet another parametric model to estimate the computational intensity of gridding component of the pipelines. The work summaries the required operational intensity for SKA1 LOW and SKA1 MID settings for different baseline lengths. This can be used as a reference to check if our benchmarks reach the required performance.

http://ska-sdp.org/sites/default/files/attachments/sdp-gridding-computational_part_1_-_signed.pdf

**Distributed imaging I/O prototype:**

This work presents facet based imaging using distributed FFTs approach. The prototype was build primarily to stress the I/O of the HPC system.

http://ska-sdp.org/sites/default/files/attachments/distributed_predict_io_prototype_part_1_-_signed.pdf

More rabbit hole details of the approach can be found in a more (unpublished) paper

http://www.mrao.cam.ac.uk/~pw410/wtowers_paper.pdf

**Parallel file system benchmarks:**

This document presents some of the results from benchmarking the file systems like LUSTRE, IBM Spectrum Scale and BeeGFS using micro benchmarks.

http://ska-sdp.org/sites/default/files/attachments/ska-tel-sdp-0000061_01c_rep_sdpmemoispreport_-_signed.pdf

**Baseline dependent averaging:**

This work would be of interest to radio-astronomers as it addresses the question of the using baseline dependent averaging in the SKA context.

http://ska-sdp.org/sites/default/files/attachments/ska-tel-sdp-0000017_01c_rep_sdpmemobaselinedependentaveraging_-_signed.pdf

**Primer on gridding:**

This memo serves as a nice primer on gridding technique used in radio-astronomy. This is relevant for the ones that are not very familiar with the technique. Besides, it provides a state-of-the-art gridding algorithms (even though the memo is little bit outdated).

http://ska-sdp.org/sites/default/files/attachments/sdp_memo_085_pip.img_gridding_algorithms_part_1_-_signed_0.pdf

These are some of the relevant works in terms of co-design and benchmark activities. There are plenty more available and can be found here:

http://ska-sdp.org/publications/released-sdp-memos-1

http://ska-sdp.org/publications/released-sdp-memos-2

**Code repositories:**

**Radio-astronomy Simulation, Calibration and Imaging Library (RASCIL):**

This code base is developed to fit in the SDP architecture entirely in Python using DASK for parallelism. The code is scalable to the size of SKA1 LOW and SKA1 MID settings, but not in terms of performance. It is being developed by SIM team and the repository can be found at

https://gitlab.com/ska-telescope/external/rascil

The following repository provides some use cases of RASCIL library

https://gitlab.com/ska-telescope/external/rascil-examples

**Imaging Domain Gridder (IDG):**

The IDG gridder developed within ASTRON/LOFAR can be found here:

https://gitlab.com/ska-telescope/external/sdp-idg-gridder

**WSClean:**

WSClean is a wide field imager developed by Andre Offringa and it is being used at SKAO.

https://gitlab.com/aroffringa/wsclean

**YANDASOFT:**

This software is being developed by YANDA team at CSIRO. The repository is not part of SKA at the moment and it can be found at:

https://bitbucket.csiro.au/projects/ASKAPSDP/repos/yandasoft/browse

**NIFTY gridder:**

This gridder is being developed by NZAPP team using CUDA. It claims to follow the CPU implementation of https://gitlab.mpcdf.mpg.de/ift/nifty_gridder. This can be another potential co-design/benchmark activity here as both CPU and GPU versions of the code base is available. The repository can be found at:

https://gitlab.com/ska-telescope/sdp/ska-gridder-nifty-cuda
