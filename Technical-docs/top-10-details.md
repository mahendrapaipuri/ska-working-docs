# Top 10 Supercomputer File Systems

| **Machine**        | **File system**           |
| ------------- |:-------------:|
| Fugaku, Japan     | LUSTRE at level 2, SSDs on compute nodes at level 1 |
| SUMMIT, US      | GPFS 2.5 TB/s      |
| Sierra, US | LUSTRE + GPFS      |
| Sunway TaihuLight, Chine | - |
| Selene, GPU based, US | Flash Storage |
| Tianhe-2, China | LUSTRE |
| JUWELS Booster, Germany | GPFS |
| HPC5, Italy | - |
| Frontera, US | LUSTRE |
| Dammam-7, Saudi Arabia | - |
