# List of PRACE Production Systems

The deadline for next preparatory call for Type C access is March 1st 1100 CET.

## Summary of File Systems used
| Machine        | File system           | Compute cores |
| ------------- |:-------------:| :-------------:|
| Joliot-Curie - Rome      | LUSTRE | 293 376 |
| JUWELS      | GPFS      |  122 768 |
| HAWK | LUSTRE      | 720 896 |
| Super MUC | GPFS | 311 040 |
| Marconi | GPFS | 153 024|
| Marenostrum | GPFS | 165 888 |

## Summary of the machines:

- HAWK: AMD CPU based, offers Lustre
- Joliot-Curie SKL, JUWELS Cluster, SuperMuc-NG and MareNostrum  all use similar Intel Skylake CPU based architecture, Joliot-Curie SKL offers Lustre, JUWELS, MareNostrum and, SuperMuc-NG offers GPFS
- Joliot-Curie KNL Intel KNL based architecture, also Lustre
- Marconi100, PizDaint and JUWELS Booster all offer GPU based systems, so these are only relevant if your application offers any GPU based capabilities.


## Joliot-Curie (GENCI) - Très Grand Centre de Calcul (TGCC) - CEA Paris - France

### SKL (standard x86)
- 1 656 compute nodes, each with Intel Skylake 8168 24-core 2.7 GHz dual processors, for a total of 79 488 compute cores and 6.86 PFlop/s peak performance
- 192 GB of DDR4 memory per node – (4GB/core)
- InﬁniBand EDR 100 Gb/s interconnect

### KNL (manycore x86)
- 828 Intel KNL 7250 nodes each with a 1.4 GHz, 68-core processor, for a total of 56 304 compute core and 2.52 PFlops peak performance
- 96 GB of DDR4 memory per node and 16 GB MCDRAM memory per node
- BULL BXI high speed interconnect

### Rome (standard x86)
- 2 292 nodes with two 64-core AMD Epyc 2nd gen (Rome) processors, 2.5 GHz, 2 GB/core, for a total of 293 376 compute cores and 11.75 PFlops peak performance
- 256 GB of DDR4 memory per node
- Infiniband HDR 100 Gb/s interconnect

Access to a 500 GB/s multi level LUSTRE ﬁlesystem.

## JUWELS - Jülich Supercomputing Centre (JSC) - Leipzig, Germany

### 2271 standard compute nodes
- 2× Intel Xeon Platinum 8168 CPU, 2× 24 cores, 2.7 GHz 96 (12× 8) GB DDR4, 2666 MHz
- InfiniBand EDR (Connect-X4)
- Intel Hyperthreading Technology (Simultaneous Multithreading)
- diskless

### 240 large memory compute nodes
- Intel Xeon Platinum 8168 CPU, 2× 24 cores, 2.7 GHz
192 (12× 16) GB DDR4, 2666 MHz
- InfiniBand EDR (Connect-X4)
- Intel Hyperthreading Technology (Simultaneous Multithreading)
- diskless

### 56 accelerated compute nodes
- 2× Intel Xeon Gold 6148 CPU, 2× 20 cores, 2.4 GHz
192 (12× 16) GB DDR4, 2666MHz
- 2× InfiniBand EDR (Connect-X4)
- Intel Hyperthreading Technology (Simultaneous Multithreading)
- 4× NVIDIA V100 GPU, 16 GB HBM
- diskless

350 GB/s JUST Storage access. IBM Spectrum Scale (GPFS) parallel file system (formerly GPFS)

## HAWK (HLRS) - Stuttgart, Germany
### System Type: Hewlett Packard Enterprise Apollo
- Number of compute nodes	5,632
- AMD EPYC™ 7742 2.25 Ghz, 2 CPUs/node, 64 cores/CPU, 1.44 PB Memory
- System peak performance	26 Petaflops
- 200 GB/s Infiniband

LUSTRE file system for IO performance on scratch drive.

## SUPERMUC-NG, GCS@LRZ, GERMANY
### SuperMUC-NG
 - Intel Xeon ("Skylake")
 - 6,336 Thin compute nodes each with 48 cores and 96 GB memory
 - 144 Fat compute nodes each 48 cores and 768 GB memory per node
 - 26.9 PFLOPS/sec peak performance
 - 100 GB/s OmniPath interconnect

### Linux Cluster
- CooLMUC-2 Cluster with 28-way Haswell-based nodes and FDR14 Infiniband interconnect, used for both serial and parallel processing
- Intel Broadwell based 6 TByte shared memory server HP DL580 "Teramem"
- CooLMUC-3 Cluster with 64-way KNL 7210-F many-core processors and Intel Omnipath OPA1 interconnect, for parallel/vector processing
- IvyMUC Cluster with 8-way Ivy Bridge-based nodes and FDR14 Infiniband interconnect, used for parallel processing

200 GiB/s GPFS file systems on scratch directory for IO performance.

## MARCONI, CINECA, ITALY
### MARCONI (A1, A2, A3)
- Intel SkyLake 2x Intel Xeon 8160 @2.1GHz 24 cores each
- Nodes 1512+792+912, 192 GB RAM per node
- Peak Performance: about 2+11+10 PFlop/s
- Intel Omnipath, 100 Gb/s.

GPFS (General Parallel File System) on Scratch directory.

## MARENOSTRUM 4, BSC, SPAIN
### Marenostrum 4
- 2x Intel Xeon Platinum 8160 24C at 2.1 GHz
- 216 nodes with 12x32 GB DDR4-2667 DIMMS (8GB/core)
- 3240 nodes with 12x8 GB DDR4-2667 DIMMS (2GB/core).
- 3,456 nodes, 390 Terabytes memory, 11.15 Petaflops peak performance

100Gb Intel Omni-Path Full-Fat Tree
IBM General Parallel File System (GPFS)

## PIZ DAINT, ETH ZURICH/CSCS, SWITZERLAND
### Piz Daint
- This machine is NOT AVAILABLE FOR TYPE C ACCESS.
- Intel® Xeon® E5-2690 v3 @ 2.60GHz (12 cores, 64GB RAM) and NVIDIA® Tesla® P100 16GB - 5704 Nodes
- Two Intel® Xeon® E5-2695 v4 @ 2.10GHz (2 x 18 cores, 64/128 GB RAM) - 1813 Nodes
- Aries routing and communications ASIC, and Dragonfly network topology

Sonexion 3000 Parallel File System Peak Performance	112 GB/s, Sonexion 1600 Parallel File System Peak Performance	138 GB/s. (Defacto LUSTRE?)
