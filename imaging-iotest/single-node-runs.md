# Imaging IO test on a Single Node

The prototype is run on single node to check the optimal number of streamer processes and OpenMP threads for a given number of physical cores. Peter suggested to use number of facets + 1 as number of facet workers and number of NUMA nodes as number of streamer workers. OpenMP threads are chosen based on streamer workers in a way not to exceed the number of physical cores. For example, on a node of 32 physical cores, if 2 NUMA nodes are available, 2 streamer processes and 32/2=16 OpenMP threads are used.

The number of streamer processes are varied along with OpenMP threads to find an optimal balance. For the same case of a node with 32 physical cores, following combinations are chosen:
- 2 streamer processes, 16 OpenMP threads
- 4 streamer processes, 8 OpenMP threads
- 8 streamer processes, 4 OpenMP threads
- 16 streamer processes, 2 OpenMP threads

Configuration used for the benchmark is
```
mpirun -np {} ./iotest --facet-workers=10 --rec-set=16k-8k-512 \
			 --vis-set=midr5 --time=-290:290/1024/128 --freq=0.35e9:0.4725e9/8192/128
       --dec=-30 --source-count=10 --send-queue=4 --subgrid-queue=16 \
			 --bls-per-task=8 --task-queue=32
```

Runtime on the master node is presented for each run. Runtime is measured between `MPI_Init` and `MPI_Finalize`. Thus, it includes all the communication overheads. All runs are repeated 3 times and average runtime is reported.

## Run on Dahu Cluster in Grenoble

### Configuration

- 2 Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz, 16 cores/CPU on each node
- 192GB RAM
- 1 x 10Gb Ethernet, 1 x 100Gb Omni-Path

### Runtime data

| Config          | Runtimes [sec]   |
| :-------------: |:-------------:|
| **2 streamers, 16 threads (-np 12)**      | **402.71** |
| 4 streamers, 8 threads (-np 14)     | 410.44      |
| 8 streamers, 4 threads (-np 18) | 461.70      |
| 16 streamers, 2 threads (-np 26) | 510.16      |


## Run on Yeti Cluster in Grenoble

### Configuration

- 4 Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz, 16 cores/CPU on each node
- 768GB RAM
- 1 x 10Gb Ethernet, 1 x 100Gb Omni-Path

### Runtime data

| Config          | Runtimes [sec]   |
| :-------------: |:-------------:|
| 2 streamers, 32 threads (-np 12)      | 367.52 |
| **4 streamers, 16 threads (-np 14)**     | **194.64**      |
| 8 streamers, 8 threads (-np 18) | 215.91      |
| 16 streamers, 4 threads (-np 26) | 236.63      |
| 32 streamers, 2 threads (-np 42) | 292.84      |


## Run on P3

### Configuration

- 2 Intel(R) Xeon(R) CPU E5-2683 v4 @ 2.10GHz, 16 cores/CPU on each node
- 128GB RAM

### Runtime data

| Config          | Runtimes [sec]   |
| :-------------: |:-------------:|
| **2 streamers, 16 threads (-np 12)**      | **638.11** |
| 4 streamers, 8 threads (-np 14)      | 647.01      |
| 8 streamers, 4 threads (-np 18) | 729.44     |
| 16 streamers, 2 threads (-np 26) | 907.25      |

## Inferences

All tests on different clusters gave consistent results. Peter's suggestion of using as many streamers as NUMA nodes is the best configuration.
