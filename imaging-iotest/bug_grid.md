# Positivity of w in grid.c in Imaging IO test

I have looked a bit into the bug, where positivity of w fails in `_frac_coord` call in `grid.c`.

I could reproduce the issue with the following config.

```
mpirun -np 34 ./iotest --rec-set=small --facet-workers=10 --vis-set=lowbd2 --time=-460:460/1024/64 --freq=260e6:300e6/8192/64 --dec=-30 --source-count=10 --send-queue=4 --subgrid-queue=16 --bls-per-task=8 --task-queue=32 --wstep=64 --margin=32
```

on P3 running on 12 nodes. I found few things, but I am not sure about more lower levels details. I hope this can help you to figure out what is happening.

So, at the point of failure, I logged few variables that might be interesting. Here they are:

```
w = -0.9999819021015612; dw = 0.0000000003049454; w0 = -0.9999819082004693;
min_w = 0.0000305175781251;
```

First thing that caught my attention is the `w` is negative, `dw` is very small and `w < min_w`. As you are transforming the coordinates, I assume `w` has to be always positive?

Well, the code fails at the first iteration in `degrid_conv_uvw_line` function call as `frac_coord` is called inside it.

After more logging, I have noticed that `w` has negative values at lot of time steps (you are anticipating this I assume). Equally `dw` is in the order of 1e-10 for lot of time steps as well. I see that this piece of code to guard against these problems:

```
int i0 = 0, i1 = count;
if (du > 0) {
    if (u0+i0*du < min_u) i0 = imax(i0, ceil( (min_u - u0) / du ));
    if (u0+i1*du > max_u) i1 = imin(i1, ceil( (max_u - u0) / du ));
} else if (du < 0) {
    if (u0+i0*du > max_u) i0 = imax(i0, ceil( (max_u - u0) / du ));
    if (u0+i1*du < min_u) i1 = imin(i1, ceil( (min_u - u0) / du ));
}
if (dv > 0) {
    if (v0+i0*dv < min_v) i0 = imax(i0, ceil( (min_v - v0) / dv ));
    if (v0+i1*dv > max_v) i1 = imin(i1, ceil( (max_v - v0) / dv ));
} else if (dv < 0) {
    if (v0+i0*dv > max_v) i0 = imax(i0, ceil( (max_v - v0) / dv ));
    if (v0+i1*dv < min_v) i1 = imin(i1, ceil( (min_v - v0) / dv ));
}
if (dw > 0) {
    if (w0+i0*dw < min_w) i0 = imax(i0, ceil( (min_w - w0) / dw ));
    if (w0+i1*dw > max_w) i1 = imin(i1, ceil( (max_w - w0) / dw ));
} else if (dw < 0) {
    if (w0+i0*dw > max_w) i0 = imax(i0, ceil( (max_w - w0) / dw ));
    if (w0+i1*dw < min_w) i1 = imin(i1, ceil( (min_w - w0) / dw ));
}
i0 = imax(0, imin(i0, i1));
```
Effectively you are readjusting `i0` based on the `min_w` and `max_w` if  `w0` (or defacto `w`) goes out of this range.

The problem arises when `dw` is very low. If we do the math, the output of `ceil( (min_w - w0) / dw )` is `(double) 3279322397.0.` The number is too big to be casted into a `int` in `imax` macro. What compiler is doing here is giving a limit of `int` on the negative side as input to `imax` macro, which in-turn returns original `i0` as output.

So, my inference is the above piece of code couldnt handle that case of very low `dw` and negative `w0`. At least in the case of `--rec-set=small` case, this happens only at only one time step and one `uvw` coordinate.

One dirty (quick) fix I tried is changed the argument types of `imax` macro to `long int` and it worked.

Finally, I dont know if `dw` is supposed to be so low. If that is the problem, then the bug might be a deeper issue. On the other hand, as I said, there were quite a few time steps that give low `dw` values but in those cases, `w0` is positive and greater than `min_w`, so code doesnt raise any errors.

I dont know if it is any useful but, I thought it is worth digging into it before testing the code on production machines.
