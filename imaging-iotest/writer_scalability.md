# Run on p3

This is the one of the writer statistics from the p3 run that you pointed out yesterday that it ran as expected.

```
Tue Mar 23 15:48:26 2021[1,1]<stdout>:Finishing writes...
Tue Mar 23 15:48:29 2021[1,1]<stdout>:Streamed for 8181.05s
Tue Mar 23 15:48:29 2021[1,1]<stdout>:Received 0.12 GB (26 subgrids, 11286 baselines)
Tue Mar 23 15:48:29 2021[1,1]<stdout>:Receiver: Wait: 7813.74s, Recombine: 0.11484s, Check: 289.488s, Idle: 77.7137s
Tue Mar 23 15:48:29 2021[1,1]<stdout>:Worker: Wait: 122893s, FFT: 65.1618s, Degrid: 3071.32s, Idle: -3236.34s
Tue Mar 23 15:48:29 2021[1,1]<stdout>:Operations: degrid 4.6 GFLOP/s (32013786551/1097364144128 visibilities, 512.22 GB, rate 0.06 GB/s, 1351585 chunks)
Tue Mar 23 15:48:29 2021[1,1]<stdout>:Grid accuracy: RMSE 2.27799e-08, worst 5.51891e-08 (1248 samples)
Tue Mar 23 15:48:29 2021[1,1]<stdout>:Grid wmax accuracy: RMSE 2.278e-08, worst 5.34928e-08 (1248 samples)
Tue Mar 23 15:48:29 2021[1,1]<stdout>:Vis accuracy: RMSE 1.16916e-05, worst 0.000112382 (5342945 samples)
Tue Mar 23 15:48:29 2021[1,1]<stdout>:Writer 6: 5331621415 visibilities (85.31 GB, written 220.71 GB, rewritten 38.04 GB), rate 0.03 GB/s (0.01 GB/s effective)
Tue Mar 23 15:48:29 2021[1,1]<stdout>:Writer 6: Wait: 240.9s, Read: 684.684s, Write: 7206.21s, Idle: 49.2635s
Tue Mar 23 15:48:29 2021[1,1]<stdout>:Writer 7: 5320276244 visibilities (85.12 GB, written 219.54 GB, rewritten 37.44 GB), rate 0.03 GB/s (0.01 GB/s effective)
Tue Mar 23 15:48:29 2021[1,1]<stdout>:Writer 7: Wait: 192.793s, Read: 692.443s, Write: 7198.56s, Idle: 97.2589s
Tue Mar 23 15:48:29 2021[1,1]<stdout>:Writer 8: 5370776871 visibilities (85.93 GB, written 220.69 GB, rewritten 37.56 GB), rate 0.03 GB/s (0.01 GB/s effective)
Tue Mar 23 15:48:29 2021[1,1]<stdout>:Writer 8: Wait: 144.322s, Read: 689.462s, Write: 7201.61s, Idle: 145.659s
Tue Mar 23 15:48:29 2021[1,1]<stdout>:Writer 9: 5391862732 visibilities (86.27 GB, written 220.20 GB, rewritten 36.84 GB), rate 0.03 GB/s (0.01 GB/s effective)
Tue Mar 23 15:48:29 2021[1,1]<stdout>:Writer 9: Wait: 47.2695s, Read: 675.301s, Write: 7215.76s, Idle: 242.726s
Tue Mar 23 15:48:29 2021[1,1]<stdout>:Writer 10: 5293948637 visibilities (84.70 GB, written 219.35 GB, rewritten 37.53 GB), rate 0.03 GB/s (0.01 GB/s effective)
Tue Mar 23 15:48:29 2021[1,1]<stdout>:Writer 10: Wait: 96.4431s, Read: 634.192s, Write: 7256.8s, Idle: 193.622s
Tue Mar 23 15:48:29 2021[1,1]<stdout>:Writer 11: 5305300652 visibilities (84.88 GB, written 219.54 GB, rewritten 37.11 GB), rate 0.03 GB/s (0.01 GB/s effective)
Tue Mar 23 15:48:29 2021[1,1]<stdout>:Writer 11: Wait: 0.0713634s, Read: 622.608s, Write: 7268.45s, Idle: 289.927s
```
Here are the key stats from this run on p3:
```
Mean streamer time in sec           :  8175.618 +/- 966.948
Total degrid rate in GFLOPS/sec     :  158.900
Total degrid data rate in GB/sec    :  2.130
Total written data in GB            :  34779.530
Total written data rate in GB/sec   :  4.390
Total IO bandwidth on file system in GB/sec   :  4.679
```

Degrid rate, degrid data rate and written data are computed using streamer time. IO bandwidth is calculated using written data and writer write time. It confirms that we
are indeed getting same bandwidth as you achieved in the memo.

I was using 6 writer threads per streamer process. As you said, one writer is blocked by the IO, where other writers are waiting for the data. This trend is same for
all the streamer processes. Is this what expected? I dont know why Grafana wasnt picking this up.

# Run on JUWELS

I got a similar trend of JUWELS as well. I ran two small (around 140 GB data to write) on 8 nodes with a chunk size of 64 KiB. One test is with single writer thread and other is
with two writer threads. Here are the key results:

## For the run with single writer thread
```
Mean streamer time in sec           :  115.864 +/- 27.532
Total degrid rate in GFLOPS/sec     :  90.900
Total degrid data rate in GB/sec    :  1.220
Total written data in GB            :  355.260
Total written data rate in GB/sec   :  3.200
Total IO bandwidth on file system in GB/sec   :  5.987
```
Sample writer statistics from SLURM out:
```
0: Finishing writes...
0: Streamed for 82.71s
0: Received 0.51 GB (110 subgrids, 24313 baselines)
0: Receiver: Wait: 73.4958s, Recombine: 0.394534s, Check: 7.21877s, Idle: 1.60118s
0: Worker: Wait: 1608.53s, FFT: 145.909s, Degrid: 52.6234s, Idle: 96.8795s
0: Operations: degrid 6.2 GFLOP/s (436502511/8573157376 visibilities, 6.98 GB, rate 0.08 GB/s, 438847 chunks)
0: Grid accuracy: RMSE 2.28253e-08, worst 5.63624e-08 (5282 samples)
0: Grid wmax accuracy: RMSE 2.27737e-08, worst 6.13424e-08 (5282 samples)
0: Vis accuracy: RMSE 9.87401e-06, worst 9.63049e-05 (110150 samples)
0: Writer 0: 436502511 visibilities (6.98 GB, written 24.47 GB, rewritten 15.12 GB), rate 0.30 GB/s (0.08 GB/s effective)
0: Writer 0: Wait: 0.0788724s, Read: 35.0192s, Write: 40.7719s, Idle: 6.84033s
0: Max wall time among processes is 180.292731 s
```

## For the run with two writer threads:
```
Mean streamer time in sec           :  287.826 +/- 46.849
Total degrid rate in GFLOPS/sec     :  35.800
Total degrid data rate in GB/sec    :  0.490
Total written data in GB            :  355.280
Total written data rate in GB/sec   :  1.240
Total IO bandwidth on file system in GB/sec   :  1.857
```
Sample writer statistics from SLURM out:
```
0: Finishing writes...
0: Streamed for 237.06s
0: Received 0.29 GB (63 subgrids, 19199 baselines)
0: Receiver: Wait: 206.533s, Recombine: 0.217451s, Check: 15.1884s, Idle: 15.1207s
0: Worker: Wait: 5087.18s, FFT: 79.2505s, Degrid: 50.2498s, Idle: 250.8s
0: Operations: degrid 2.3 GFLOP/s (466113360/8573157376 visibilities, 7.46 GB, rate 0.03 GB/s, 418670 chunks)
0: Grid accuracy: RMSE 2.29989e-08, worst 5.50265e-08 (3025 samples)
0: Grid wmax accuracy: RMSE 2.25823e-08, worst 5.67353e-08 (3025 samples)
0: Vis accuracy: RMSE 1.03985e-05, worst 8.70888e-05 (105060 samples)
0: Writer 0: 231554334 visibilities (3.70 GB, written 11.84 GB, rewritten 4.86 GB), rate 0.05 GB/s (0.02 GB/s effective)
0: Writer 0: Wait: 6.63296s, Read: 52.7696s, Write: 168.775s, Idle: 8.88185s
0: Writer 1: 234559026 visibilities (3.75 GB, written 12.05 GB, rewritten 5.01 GB), rate 0.05 GB/s (0.02 GB/s effective)
0: Writer 1: Wait: 0.0528061s, Read: 51.0263s, Write: 170.498s, Idle: 15.4826s
0: Max wall time among processes is 406.849764 s
```

You can see here as well one of the writers is waiting for data. Just to finish it off, I replicated exactly same test on p3 and I got the same trends: Run with two writer threads per streamer process is considerably slower than that of single writer thread run and one of the writer threads (when using two) has bigger wait times.   
