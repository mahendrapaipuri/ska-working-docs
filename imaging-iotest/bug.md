# Positivity of grid_min_u bug in Imaging IO test

At line 107 in `streamer_work.c`,

`grid_min_u = subgrid_size/2 + theta*(min_u-mid_u)`

where `subgrid_size = recombine.xM_size`, `min_u = (subgrid_off_u - sg_step / 2) / theta`, `mid_u = subgrid_off_u / theta`. So,

`grid_min_u = recombine.xM_size/2 + (subgrid_off_u - sg_step / 2 - subgrid_off_u)`, where

`sg_step = recombine.xA_size + margin`.

Effectively, `grid_min_u = recombine.xM_size/2 - recombine.xA_size/2 - margin/2`.

When I used `rec-set=small`, `recombine.xM_size=512`, `recombine.xA_size=384`. So, with `margin=32`, it should give us `grid_min_u=48`.

However, you correct the user provided margin at line 859 in `config.c` based on subgrid spacing. That increases `margin` to 128 for the same case of `--rec-set=small`. So, we should get `grid_min_u=0`.

As `theta`, `min_u` and `mid_u` are floats, rounding off errors creeps in and `grid_min_u` becomes something like `-1e-8`. I dont know if `grid_min_u = 0` is acceptable as in the `assert` condition, you used strictly greater than zero.

MAYBE HERE -0.9999819021015612 0.0000000003049454 -0.9999819082004693 0.0000305175781251 -0.9999819017966158 20 64 20 3279316527.000000
printf("MAYBE HERE %.16f %.16f %.16f %.16f %.16f %d %d %d %f\n", w, dw, w0, min_w, w+dw, i0, i1, imax(i0, ceil( (min_w - w0) / dw )), \
ceil( (min_w - w0) / dw ))

280312500.0000000000000000 280317382.8125000000000000 -2949955.4708031020127237 -2949955.4683235883712769


/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>
#include <math.h>
inline static long int imax(long int a, long int b) { return a >= b ? a : b; }
int main()
{
    double w0 = -0.9999837622683821;
    double min_w = 0.0000305175781251;
    double dw = 0.0000000003049454;
    int i0 = 0;

    double b = ceil((min_w - w0) / dw);
    long int bb = (long int) b;

    double x = 23.00000;
    int y = (int) x;

    if (w0+i0*dw < min_w) i0 = imax(i0, b);

    printf("%ld %f %ld\n", i0, b, bb);

    return 0;
}
