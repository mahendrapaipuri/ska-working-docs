# Scalability Tests of Imaging IO Benchmark on JUWELS (JSC)

# Context

The Imaging I/O test is run on JUWELS machine to analyse the I/O performance at a scale of more than 100 compute nodes. For all the tests reported here, `batch` partition of the JUWELS is used to run the tests.

# Hardware and software configurations

All the compute nodes are equipped with 2 sockets with 24 cores per each socket and hyper threading enabled. The layout the compute node is shown as follows:

<p align="center">
  <img src="imgs/juwels-compute-node.png" width="600">
</p>

Other configuration details of the `batch` partition are:

- 2× Intel Xeon Platinum 8168 CPU, 2× 24 cores, 2.7 GHz 96 (12× 8) GB DDR4, 2666 MHz.
- InfiniBand EDR (Connect-X4).
- Intel Hyperthreading Technology (Simultaneous Multithreading).
- diskless.
- 250 GB/s network connection to JUST for storage access realised by IBM Spectrum Scale (GPFS).

All the compute nodes are CentOS 8 Linux distribution and SLURM is used for batch scheduling system. The cluster software stack provides Intel MPI, Open MPI and ParTec ParaStation MPI implementations. For the current work, **only OpenMPI** is used. The default `HDF5` module provided by the JUWELS software stack do not support thread safe configuration. Without thread safety feature, we cannot use more than one writer thread per worker process and thus, limiting the I/O bandwidth we can use. So, I build local `HDF5` modules using `easybuild` package with thread safety feature enabled. JSC provides the `easybuild` package as a part of their default software stack and it is used to build the local module with the following configuration file:

```
# This file is part of JSC's public easybuild repository (https://github.com/easybuilders/jsc)
name = 'HDF5'
version = '1.10.6'
versionsuffix = '-serial'

homepage = 'http://www.hdfgroup.org/HDF5/'
description = """HDF5 is a unique technology suite that makes possible the management of
 extremely large and complex data collections.
"""


toolchain = {'name': 'GCCcore', 'version': '9.3.0'}
toolchainopts = {'optarch': True, 'pic': True}

source_urls = [
    'https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-%(version_major)s.%(version_minor)s/hdf5-%(version)s/src'
]
sources = [SOURCELOWER_TAR_GZ]
checksums = ['5f9a3ee85db4ea1d3b1fa9159352aebc2af72732fc2f58c96a3f0768dba0e9aa']

builddependencies = [
    ('binutils', '2.34')
]

dependencies = [
    ('zlib', '1.2.11'),
    ('Szip', '2.1.1'),
]

moduleclass = 'data'

# easyblock = 'ConfigureMake'

configopts = '--enable-unsupported --enable-threadsafe'

```

Another important finding here is the HDF5 library do not really work as expected when multiple threads are writing concurrently even to different files. As found from the HDF5 forum discussions, HDF5 library has a global lock that prohibits multiple threads to do concurrent write operations. It is found with some tests that this is the case, where having more than 1 writer thread created from same process actually degrades the I/O performance. The solution to this is to actually use full processes rather than threads. It involves forking the process and writing the data concurrently with different processes. `OpenMPI` complains about forking the process and sometimes generating segfaulting code. This is due to inconsistencies between OpenFabrics libraries and OpenMPI's (thats what I understood). On JUWELS, the interconnects are managed by UCX libraries and `OpenMPI` did not complain about forking on this particular occasion. Thus, for these runs, full processes are used to write the data concurrently rather than threads.  

## SLURM configuration

The optimal number of MPI processes in the Imaging I/O test code depends on the number of facets in the input image. Thus, number of MPI processes do not necessarily scale linearly with number of compute nodes. I figured out that it is optimal to reserve all the resources the compute nodes have at the `#SBATCH` commands and then use these reserved resources with the `srun` command as per our needs. For example, if we are using 2 compute nodes with 48 cores (physical) each and we plan to use 12 MPI processes (with 24 OpenMP threads), it is better to have a SLURM configuration as follows:

```
#SBATCH --nodes=2
#SBATCH --ntasks=192
...

export OMP_NUM_THREADS=24
srun --ntasks=12 --cpus-per-task=24 ...
```

Moreover, if the `--ntasks` and `--cpus-per-task` are defined at the `#SBATCH`, SLURM complains that it doesnt have enough resources to launch the job, which is true. But in this particular case of Imaging I/O test, all the producer processes do not actually do any computation. Their job is to keep the image data in the memory and send it to the streamer workers, where all the compute and I/O work is done. Thus, the producer workers remain idle most of the time and they should not counted towards final workload. For more information on different types of workers in the code, consult this [memo](http://ska-sdp.org/sites/default/files/attachments/distributed_predict_io_prototype_part_1_-_signed.pdf) and [GitLab repository](https://gitlab.com/ska-telescope/sdp/ska-sdp-exec-iotest/-/tree/bench-work).

A sample SLURM script for the run with 128 nodes is as follows:

```
#!/bin/bash

#SBATCH --time=01:00:00
#SBATCH --job-name=sdp-benchmarks
#SBATCH --account=training2022
#SBATCH --nodes=128
#SBATCH --ntasks=12288
#SBATCH --partition=batch
#SBATCH --output=/p/project/training2022/paipuri/sdp-benchmarks/io_bench/out/juwels-io-scalability-bare-metal-128-small-lowbd2-256-256-0.out
#SBATCH --error=/p/project/training2022/paipuri/sdp-benchmarks/io_bench/out/juwels-io-scalability-bare-metal-128-small-lowbd2-256-256-0.out
#SBATCH --mail-type=FAIL
#SBATCH --no-requeue
#SBATCH --exclusive

# GENERATED FILE

set -e

# Purge previously loaded modules
module purge
# Load GCC OpenMPI HDF5/1.10.6-serial FFTW/3.3.8 git, git-lfs modules
module load GCC/9.3.0 OpenMPI/4.1.0rc1 FFTW/3.3.8 git-lfs/2.12.0 git/2.28.0 zlib/1.2.11-GCCcore-9.3.0 HDF5/1.10.6-GCCcore-9.3.0-serial

# Give a name to the benchmark
BENCH_NAME=juwels-io-scalability

# Directory where executable is
WORK_DIR=/p/project/training2022/paipuri/sdp-benchmarks/io_bench/ska-sdp-exec-iotest/src

# Any machine specific environment variables that needed to be given.
# export UCX log to suppress warnings about transport on JUWELS
# (Need to check with JUWELS technical support about
# this warning)
export UCX_LOG_LEVEL=ERROR

# Any additional commands that might be specific to a machine


# Change to script directory
cd $WORK_DIR

echo "JobID: $SLURM_JOB_ID"
echo "Job start time: `date`"
echo "Job num nodes: $SLURM_JOB_NUM_NODES"
echo "Running on master node: `hostname`"
echo "Current directory: `pwd`"

echo "Executing the command:"
CMD="export OMPI_MCA_mpi_yield_when_idle=1 \
  OMP_PROC_BIND=true OMP_PLACES=sockets OMP_NUM_THREADS=24 && srun --distribution=cyclic:cyclic:fcyclic \
  --cpu-bind=verbose,sockets --overcommit --label --nodes=128 --ntasks=266 --cpus-per-task=24 ./iotest \
  --facet-workers=10 --rec-set=small --vis-set=lowbd2 --time=-460:460/1024/256 --freq=260e6:300e6/8192/256 --dec=-30 \
  --source-count=10 --send-queue=4 --subgrid-queue=16 --bls-per-task=8 --task-queue=32 --fork-writer --wstep=64 \
  --margin=32 --writer-count=8 \
  /p/scratch/training2022/paipuri/sdp-benchmarks-scratch/juwels-io-scalability-bare-metal-128-small-lowbd2-256-256-0/out%d.h5\
   && rm -rf \
  /p/scratch/training2022/paipuri/sdp-benchmarks-scratch/juwels-io-scalability-bare-metal-128-small-lowbd2-256-256-0/out*.h5"

echo $CMD

eval $CMD

echo "Job finish time: `date`

```

The above configuration is used for the all runs reported here, albeit, with different number of nodes.

## Scalability results

Due to the limited compute time available on the JUWELS cluster, only bare metal deployments are used. The runs are realised on `[8, 16, 32, 64, 128]` compute nodes. The speed up on different parameters is computed based on the metrics of the 8 node run. This is not an ideal baseline but, a run on 1 node is not very interesting use case for this particular application. Nevertheless, the idea of this test is to see if the underlying file system's I/O bandwidth can keep up with the rate the data is being produced by the compute part. To this extent, both dry (without writing data) and non-dry runs are realised. Two important parameters are analysed here namely, degrid data rate and mean stream time. Degrid data rate (in GB/s) quantifies how fast the image data is being converted into visibility data. Mean stream time is the average of all streamer processes wall time. This includes time taken for FFT, degridding and writing data to the disk.

The results are shown below:

<p align="center">
  <img src="imgs/Speed_up_juwels.png" width="1000">
</p>

Each run is repeated 3 times here. For the case of dry runs, mean of 3 runs are reported in the plots. Whereas, for the case non-dry runs, best of 3 runs are chosen. At JSC, there is no way to monitor the state of the file system for the end users and the I/O bandwidth can depend on how many users are accessing the file system for I/O operations at that point of time. Thus, the best of 3 runs are chosen to present the results in the non-dry cases. In any case, there are not big differences observed between different runs both for dry and non-dry cases.  

It is clear that the dry runs scale very well up to 128 nodes. A degrid rate of more than 1024 GB/s is achieved for the dry runs with 128 nodes, whereas a mere 64 GB/s is obtained on the non-dry run. This clearly suggests the Imaging I/O test is I/O bound. Similarly, the scalability of the non-dry runs is inferior compared to the dry ones. Another interesting metric to notice here is the average I/O bandwidth achieved in different runs. It is clear from the plot that more than 100 GB/s is obtained for the run with 128 nodes. This is not in agreement with the degrid data rate, where only 64 GB/s is obtained. Essentially the data coming out of the degrid step has to be written to the disk and so, if I/O bandiwth of 100 GB/s is obtained one can think a similar degrid rate has to be achieved. The difference in these two rates is due to the numerous rewrites that the benchmark will do in the current configuration.

As already stated, JUWELS JUST file system is realised with IBM Spectrum Scale with a block size of `16 MiB`. Ideally, I/O operations should be multiple of this block size to achieve good bandwidths. The Imaging I/O code, in its current implementation, is not ready to write in such a big block size. For the current work, a block size of `1 MiB` is used, which is relatively big. When a bigger block size is used, there will be lot of overlaps in the subgrids which eventually translates to lot of rewrites. For instance, the number of time steps and frequency channels used in the current configuration should produce approximately `17 TB` of visibility data. The I/O bandwidth with time is shown below for the a run with 128 nodes obtained from JUWELS job report is shown below:

<p align="center">
  <img src="imgs/IO_bandwidth_128.png" width="1000">
</p>

It is clear that more than `32 TB` of data had been written to the disk, which is result of rewriting the visibility data by multiple writers. At the same time, the achieved I/O bandwidth also depends on this block size. In the current tests, we achieved an I/O bandwidth of approximately 800 - 900 MB/s per node. This is a direct consequence of using a smaller block size than the one configured on the file system. To verify this, an I/O bandwidth benchmark is realised using `fio` tool. Four different scenarios are used to benchmark the I/) bandwidth on JUST file system.

- A block size of `1 MiB` with 16 parallel writers. This case corresponds to the configuration used for Imaging I/O test code. Each node has 16 writers with a block size of `1 MiB`.
`fio --name=randwrite --ioengine=posixaio --iodepth=1 --rw=randwrite --bs=1M --direct=0 --size=50G --numjobs=16 --runtime=240 --group_reporting`
- A block size of `8 MiB` with 16 parallel writers.
`fio --name=randwrite --ioengine=posixaio --iodepth=1 --rw=randwrite --bs=8M --direct=0 --size=50G --numjobs=16 --runtime=240 --group_reporting`
- A block size of `16 MiB` with 16 parallel writers. This is supposed to be the best case scenario for the configured block size on JUST file system.
`fio --name=randwrite --ioengine=posixaio --iodepth=1 --rw=randwrite --bs=16M --direct=0 --size=50G --numjobs=16 --runtime=240 --group_reporting`
- And finally with a bloc size of `32 MiB` with 16 parallel writers.
`fio --name=randwrite --ioengine=posixaio --iodepth=1 --rw=randwrite --bs=32M --direct=0 --size=50G --numjobs=16 --runtime=240 --group_reporting`


| Config          | Mean bandwidth in MB/s    | Max bandwidth in MB/s |
| :-------------: |:-------------:|:-------------:|
| 1 MiB      | 1080 | 1132 |
| 8 MiB     | 3923      | 4114 |
| 16 MiB | 5890      | 6120 |
| 32 MiB | 5745      | 5980|

These benchmark numbers clearly indicate a need to have a block size used by the Imaging I/O code to be a integral of the block size of the underlying file system to get optimal I/O bandwidth performance. IBM Spectrum Scale do not allow for the end users to configure the block size. This is configured for the entire file system and cannot be changed without re-deploying the file system. On the other hand, LUSTRE can come handy where stripe size and stripe count on LUSTRE file system can be configured for each file and directory at the user level.

## Final remarks

The I/O bandwidths achieved by the Imaging I/O test code are in agreement with the benchmark results and we obtained the maximum bandwidth that we could using the current configuration. Several directions can be envisioned at this point. One would be to aggregate the visibility chunks before writing the data to the disk. If we can make it configurable, we can degrid using smaller chunks (which involves less rewrites) and then aggregate multiple of these visibility chunks before writing it to the file system.
