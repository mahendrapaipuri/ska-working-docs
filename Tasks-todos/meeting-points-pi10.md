# Points to discuss for PI 10
## Meeting with Miles, Verity, Cees

### PRACE compute time

- Type B access is more suitable to our needs
- Its a continuous call, so no pressure on deadline
- Multiple machines can be requested if we justify the needs
- Told to mention the SKA-PRACE-CERN-GEANT collaboration in the proposal

### Questions to PLANET team

- Preferred start date
- Computing Resources


### Questions to Peter

- Total storage requirements - 20 TB for biggest image ?
- Describe possible solutions you have considered to improve the performance of the project
- Describe the application enabling/optimization work that needs to be performed to achieve the target performance
