# PRACE Proposal Draft - v0.1
## Mahendra Paipuri

### Project name

SKA SDP Imaging Pipeline Benchmark Experiments

### Research Field

Universe Sciences

### Requested Computer Systems

To be decided

### Summary of the Project

The context of the current proposal comes from the Square Kilometre Array (SKA) project, which is an international effort to build the world's largest radio telescope. The processed signal data from the antennas will reach the Science Data Processor (SDP), where different workflows will run. The initial estimates require the SDP to operate at a consistent rate of 250 PFlops and eventually move the reduced data to persistent storage at an annual rate of 600 PB. Thus, SDP will involve high-performance computing along with intense requirements in terms of internal data throughput. Several software prototypes were or being made to evaluate the hardware and software design solutions in the light of potential bottlenecks. The goal of the current project is to benchmark these prototypes on different architectures, heterogenous machines, file systems, _etc_., in order to identify potential bottlenecks.


### Objectif de la demande

One of the prototypes we are currently working on is an I/O bound benchmark. The prototype was developed in plain C with MPI for the communication. The final stage of the workflow is to write the data (in HDF5 format) to the file system and this is a clear bottleneck. Our primary aim is to test this prototype on LUSTRE file system on TGCC to evaluate its scalability up to at least 128 nodes. The prototype can generate more than 40 TB of data divided among several hundreds of files in certain configurations. Our previous tests on IBM Spectrum Scale were rather constrained due to the system wide fixed block size it uses. It also confirmed that we need to use custom stripe size (which LUSTRE provides) to achieve high I/O bandwidth with our prototype. Currently the prototype writes the data in chunks where we can configure the size of this chunk. We would like to evaluate the effect of this chunk size on the achieved I/O bandwidth by changing the chunk size on prototype code and stripe size of LUSTRE file system in tandem. 

### Scientific case of the project (Explain the scientific case for which you intend to use the code)

The most critical part of interferometry is processing the raw data from the telescopes to produce sky images. These images can help to study several scientific questions dubbed as Key Science Projects (KSP) in the context of the SKA project. The SKA has the unique potential to study the extrasolar terrestrial planet formation, detect signals from other life forms, finding a pulsar around a black hole, which will yield the first measurements of relativistic gravity in the ultra-strong-field limit. Radio astronomy is uniquely placed in its capability to study magnetic fields at large distances, through studies of Faraday rotation, polarized synchrotron emission and the Zeeman effect. The SKA could measure rotation measures for very large number of polarized extragalactic sources across an entire hemisphere. The SKA will provide the only means of studying the cosmic evolution of neutral Hydrogen (HI) which, alongside information on star formation from the radio continuum, is needed to understand how stars formed from gas within dark-matter over-densities. The underlying KSPs dictate overall design specifications of SKA.


### Describe the impact of the optimization work proposed

Answer ALL the following points:

- Explain how the optimization work proposed will contribute to future Tier-0 projects.

Most of the benchmark experiments involve only compute-intensive codes, where CPUs are vigorously tested. The current benchmark sheds light on I/O storage performance on Tier-0 machines in addition to compute performance. SKA-SDP poses a unique requirement, where data and compute throughputs need to achieve similar performance to avoid the potential loss of data.

- Is the code widely used?

No, this/these prototypes are especially developed for the needs of SKA-SDP works.

- Can the code be used for other research projects, with minor modifications?

Yes, the code is available on the Github/GitLab platforms and it can be forked for other research projects.

- Can the code be used in other research fields?

Yes, if the developed pipelines are relevant

- Would the modification be easy to add to the main release of the software?

Yes.



### Computer resources required

Total storage required: 30 TB (?)

Maximum amount of memory per core:


### Details of main simulation application

Name and version: imaging-iotest v0.1

Webpage: https://gitlab.com/scpmw/imaging-iotest/-/tree/master

License: Apache v2.0

### Describe the main algorithms and how they have been implemented and parallelized

The process of transforming the visibilities that come from signal processor to images is done via iterative imaging techniques based on the family of CLEAN algorithms. A basic CLEAN algorithm takes measured visibilities and turns them into a residual image using gridding and inverse Fourier Transforms (iFFT). This is referred to as the "imaging" step. Using this residual image, a sky-image is calibrated by extracting brightest source from the residual image. Thereafter, FFT and degridding is used on this sky-image to produce model visibilities. This step is called "predict" and it is dual to "imaging" step. This iterative algorithm stops when model visibilities are within a given tolerance to measured visibilities.

The current prototype focuses on the "predict" step of the iterative imaging algorithm. A sky-image is provided as input and the prototype produces the so-called visibilities as the output. The benchmark has two kinds of processes, called "producers" and "streamers." The producer worker keeps the input image in the memory and sends sub-grids of images to the streamer workers. Then, the streamer workers receive the sub-grid parts of these images, perform degridding to produce visibilities, and eventually write the storage's visibility data. MPI is used for sending and receiving the data between two different worker types. The streamer employs three types of OpenMP threads: One to receive data from the network and sum up subgrids, one to write visibilities as HDF5, and the rest to degrid visibilities from subgrids.


### Current and target performance
- Describe the scalability of the application and performance of the application

The code has been tested on Data Accelerator (DAC) system hosted at Cambridge's CSD3 system and it achieved about 1.25 GB/node/s data rate. A dry-run without writing data to the storage resulted around 2.5 GB/node/s rate.

- What is the target for scalability and performance? (i.e. what performance is needed to reach the envisaged scientific goals)

The achieved storage rates with DAC system represent roughly a fifth of the per-node data rate assumed to be required for full-scale SDP operation.

### Confidentiality

No


### Describe IO strategy regarding the parameters indicated

- Is IO expected to be a bottleneck

Yes, dry-runs achieved a data production rate of 2 to 3 times more than non-dry runs. The writing of output data to the storage system acts as the bottleneck in the current benchmark.

- Implementation: IO libraries, MPI IO, netcdf, HDF5

The current benchmark uses HDF5 libraries for writing the output. Each streamer worker creates writer threads (the number of writers is configurable) to output the data. Each writer thread creates a different output file with the current implementation. The implementation allows increasing the number of writer threads per process to keep up with high-speed storage back-ends.

- Frequency and size of output and input

All output files' total size for the biggest image in the dataset will be around 25-30 TB. On the other hand, input data is only a few hundred MBs.


- Number of files and size of each file in a typical production run

The number of files generated depends on the number of streamer processes used and the number of writer threads configured for each streamer process. Similarly, output files will vary in size as the amount of generated visibility data depends on the location of the image being worked on by a given streamer worker.


### Main performance bottlenecks

I/O storage is the main bottleneck in the present benchmark. The data writing rate cannot keep up with the data production rate from the upstream processes. Since limited queue sizes are used at each step of the algorithm, the data production rate coincides with a data writing rate shortly after all queues are saturated.

### Describe possible solutions you have considered to improve the performance

One trivial solution is to increase the number of nodes and writer threads to increase the data throughput on the storage side. The unknown is the scalability of the benchmark on a very high number of nodes. As each process writes the data separately to the storage, increasing the number of compute nodes increases the generated output files as well. If the parallel file systems show unsatisfactory scalability in handling hundreds/thousands of output files, other solutions like parallel HDF5 systems need to be explored.

### Describe the application enabling/optimization work that needs to be performed to achieve the target performance

Eventually, it is necessary to bring the prototype more in line with the SDP software architecture. Hence, to move in that direction, there are multiple good candidates here. The obvious choice given the prototype's goals might be an execution framework like StarPU, which would especially allow us to easily migrate towards doing gridding work on accelerators. However, it is not clear whether we could achieve similar guarantees with respect to total memory consumption without writing our own scheduler. Furthermore, we might attempt adapting the prototype to a more high-level execution frame-works in use by SDP, such as Dask, Apache Spark or DALiuGE. The main challenge will likely be to increase data throughput enough to achieve similar rates as the hand-coded OpenMPI solution. As for Apache Spark, there seem to be data distribution patterns that can not be implemented well in certain execution frameworks. Involving external tools such as Alluxio will likely introduce significant cost in terms of code complexity and overhead. Whether a good middle ground can be found remains to be seen.


### Which computational performance limitations do you wish to solve with this project

The current project aims at understanding both computational and I/O storage performance on large-scale production machines rather than solve them. Due to the limited available resources, extensive benchmarks could not be realised for the current prototype. Using the Tier-0 machines at PRACE can give us a clear picture of where we stand in our global objectives and what we need to improve from the algorithmic perspective.
