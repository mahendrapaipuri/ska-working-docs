# GENCI proposal

## Titre

SKA SDP Imaging Pipeline Benchmark Tests

## Thematique

I didnt find relevant subject. Please mark the appropriate one.

## Description scientifique du projet

### Résumé scientifique

The context of the current proposal comes from the Square Kilometre Array (SKA) project, which is an international effort to build the world's largest radio telescope. The processed signal data from the antennas will reach the Science Data Processor (SDP), where different workflows will run. The initial estimates require the SDP to operate at a consistent rate of 250 PFlops and eventually move the reduced data to persistent storage at an annual rate of 600 PB. Thus, SDP will involve high-performance computing along with intense requirements in terms of internal data throughput. Several software prototypes were or being made to evaluate the hardware and software design solutions in the light of potential bottlenecks. The goal of the current project is to benchmark these prototypes on different architectures, heterogenous machines, file systems, _etc_., in order to identify potential bottlenecks.


### Objectif de la demande

One of the prototypes we are currently working on is an I/O bound benchmark. The prototype was developed in plain C with MPI for the communication. The final stage of the workflow is to write the data (in HDF5 format) to the file system and this is a clear bottleneck. Our primary aim is to test this prototype on different parallel file systems to evaluate its scalability up to at least 128 nodes. The prototype can generate more than 40 TB of data divided among several hundreds of files in certain configurations. We are primarily interested in running the prototype on both LUSTRE at TGCC and IBM Spectrum Scale at IDRIS centres. We are also interested in how the block size/stripe size on the file system effects the performance of our prototype code.

## Correspondant technique

Mahendra PAIPURI
07 78 90 64 45
mahendra.paipuri@inria.fr

## Moyens demandés

Partition Joliot-Curie/Irene SKL, Bull Skylake (CPU) (100 TB Storage space)
Partition Jean Zay CSL, HPE Cascade Lake (CPU) (100 TB Storage space)

## Environnement necessaire pour faire tourner votre code

Oui (Just select C, OpenMPI, HDF5. It should be enough for the moment)
